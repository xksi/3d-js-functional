
class classGraph{

  constructor( canvasElement ){
    this.canvas = canvasElement ;
    this.maxX = this.canvas.clientWidth ;
    this.maxY = this.canvas.clientHeight ;
    this.canvas.width = this.maxX ;
    this.canvas.height = this.maxY ;
    this.ctx = this.canvas.getContext("2d");
  }

  clearScr(){
    this.ctx.fillStyle = "#000000";
    this.ctx.clearRect ( 0, 0, this.maxX, this.maxY );
  }

  triangleFilled ( x1, y1, x2, y2, x3, y3, fillStyle ){
    this.ctx.beginPath();
    this.ctx.moveTo ( x1, y1 );
    this.ctx.lineTo ( x2, y2 );
    this.ctx.lineTo ( x3, y3 );
    this.ctx.lineWidth = 1;
    this.ctx.fillStyle = fillStyle;
    this.ctx.fill();    
  }

  triangle( x1, y1, x2, y2, x3, y3, strokeStyle ){
    this.ctx.beginPath();
    this.ctx.moveTo ( x1, y1 );
    this.ctx.lineTo ( x2, y2 );
    this.ctx.lineTo ( x3, y3 );
    this.ctx.lineTo ( x1, y1);
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = strokeStyle;
    this.ctx.stroke();
  }
  
  triangle3d( x0, y0, x1, y1, x2, y2, g ){
    Graph.triangleFilled ( x0, y0, x1, y1, x2, y2, "rgba(100,"+g+",100,0.2" );
    Graph.triangle ( x0, y0, x1, y1, x2, y2, "rgba(255,"+g+",100,0.6)" );
  }

  
};


