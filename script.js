const Graph = new classGraph( document.getElementById("screen") )
const pm = { "x":0, "y":0, "z":0, "s":5 }
const xC = Graph.maxX/2
const yC = Graph.maxY/2
const myForm = document.getElementById ('formParameters') // referencja do formularza

function drawTriangles( scene, pm ){
  Graph.clearScr()
  scene.map( tr => { 
    Graph.triangle3d (
      xC + tr['pts'][0]['x'] * pm [ 's' ] * 800/(500+tr['pts'][0]['z']),
      yC + tr['pts'][0]['y'] * pm [ 's' ] * 800/(500+tr['pts'][0]['z']),
      xC + tr['pts'][1]['x'] * pm [ 's' ] * 800/(500+tr['pts'][1]['z']),
      yC + tr['pts'][1]['y'] * pm [ 's' ] * 800/(500+tr['pts'][1]['z']),
      xC + tr['pts'][2]['x'] * pm [ 's' ] * 800/(500+tr['pts'][2]['z']),
      yC + tr['pts'][2]['y'] * pm [ 's' ] * 800/(500+tr['pts'][2]['z']),
      Math.round ( 64 - tr['pts'][0]['z']/2 ) 
    )
  })
}

function rotatePt ( pt, angle, da, db, dc ){
  return {
    [db]:(pt[db] * Math.cos( angle ) - pt[da] * Math.sin( angle )),
    [da]:(pt[da] * Math.cos( angle ) + pt[db] * Math.sin( angle )),
    [dc]:(pt[dc])
  }
}

function rotateTrVrt ( vrt, pm ){ 
  return rotatePt ( rotatePt ( rotatePt ( vrt,
         pm['x'], 'y', 'z', 'x' ),
         pm['y'], 'x', 'z', 'y' ),
         pm['z'], 'x', 'y', 'z' )
}

function rotateTr ( tr, pm ){  
  return { 'pts': tr['pts'].map ( vrt => rotateTrVrt ( vrt, pm ) ) }
}

function rotate( scene, pm ){  
  return scene.map ( tr => rotateTr ( tr, pm ) );
}

function sortByZ ( scene ){
  return scene.sort ( (a,b) => ( ( a['pts'][0]['z'] > b['pts'][0]['z'] ) ? -1 : 1 ) )
}

function userParams( pm, frm ){  
  // jedyna funkcja nieczysta bo korzysta z formularza.
  return {
    'x': pm [ 'x' ] + ( frm[ 'x' ].checked ? 0.01 : 0 ),
    'y': pm [ 'y' ] + ( frm[ 'y' ].checked ? 0.01 : 0 ),
    'z': pm [ 'z' ] + ( frm[ 'z' ].checked ? 0.01 : 0 ),
    's': parseFloat( frm['s'].value )/10,
    'obj': frm['obj'].value
  }
}

function animation( scenes, pm ){  
  drawTriangles ( sortByZ ( rotate ( scenes[pm['obj']], pm ) ), pm )
  setTimeout ( ()=>{ animation( scenes, userParams ( pm, myForm ) ) }, 50 )
}

animation( scenes, userParams ( pm, myForm ) )